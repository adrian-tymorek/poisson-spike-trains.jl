# PoissonSpikeTrains.jl

A package for making simple spike train simulations based on the Poisson point process.

[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://gitlab.com/adrian-tymorek/poisson-spike-trains.jl/-/wikis/home)

## Installation

Enter the Pkg REPL by pressing `]` from the Julia REPL and type
```julia
add https://gitlab.com/adrian-tymorek/poisson-spike-trains.jl
```

## Introduction
Spike train simulations in this package are based on the [Poisson point process](https://en.wikipedia.org/wiki/Poisson_point_process), that is we assume that the neuron's activity is driven by a instantaneous firing rate $`r`$, but the onsets of individual actions potentials (further referred to as 'spikes') are random and independent of one another. Then for a homogeneous Poisson process (i.e. for a constant instantaneous firing rate $`r`$) the mean number of spikes occurring during a time interval of length $` Δt = t_2 - t_1 `$ is given by: $` n = r ~ Δt `$
and it follows Poisson distribution of mean $`r`$. Then the inter-spike interval (ISI) follows the exponential distribution, as shown below.
In practice one needs to divide a time $`T`$ into $`N`$ intervals of length $`Δt`$. Next create a random vector $`x`$ of length $`N`$ with elements $`∈ [0.0, 1.0]`$ drawn from univariate distribution. For each element of $`x[i]`$ assign 1 (meaning that at interval number of $`i`$ spike was fired) if $`x[i] ≤  r  ~ Δt`$ and 0 otherwise (no spike fired). A single spike train will look like this.
![Single spike train](./assets/homogeneous_poisson_single_spike_train.svg)


However due to its random nature a single spike train does not convey much information. Therefore one usually needs to generate multiple spike trains and stack them.
![Homogeneous Poisson raster plot](./assets/homogeneous_poisson_raster.svg)

The plots below represent the distribution of number of spikes and ISI.
![Homogeneous Poisson spike count distribution](./assets/homogeneous_poisson_spike_num_hist.svg)
![Homogeneous Poisson ISI distribution](./assets/homogeneous_poisson_isi_hist.svg)

It is possible to analogously generate inhomogeneous Poisson spike trains by allowing instantaneous firing rate to vary with time.
![Heterogeneous Poisson inst firing rate](./assets/heterogenous_poisson_inst_firing_rate.svg)

The according spike trains will look like this.
![Heterogeneous Poisson raster plot](./assets/heterogenous_poisson_raster.svg)


It is, however, an unrealistic assumption for ISI to follow exponential distribution - neurons' physiology does not allow for continuous spiking. Instead, immediately after firing a spike, neuron enters absolute refractory period, when (due to inactivation of sodium channels) it is impossible to generate an action potential. When this phase is over a relative refractory period starts, when the neuron is still less likely to fire an action potential (due to hyperpolarization of the cell membrane). These two types of refractory period may be modeled by respectively (1) setting instantaneous firing rate $`r(t)`$ immediately after firing a spike to $`0`$ and then (2) to allow it exponentially return to it's original value.


Unfortunately this way some spikes get irreversibly eliminated from a spike train and the resulting mean/instantaneous firing rate may be understated. For that reason, another model called "Renewal process', built upon Poisson model was implemented here. In order to simulate a spike train of instantaneous firing rate of $`r`$ one needs to generate a Poisson spike train with instantaneous firing rate of $`n ~ r`$ and remove all but every n-th spike. Then the number of spikes will still follow Poisson distribution of mean r, but ISI will follow [Gamma distribution](https://en.wikipedia.org/wiki/Gamma_distribution) with parameter $`k = n`$.


![Renewal process ISI distribution](./assets/renewal_process_isi.svg)



See e.g. [here](https://www.cns.nyu.edu/~david/handouts/poisson.pdf) for more information of generating Poisson spike trains.
