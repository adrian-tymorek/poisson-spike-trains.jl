module PoissonSpikeTrains
export
    Neuron,
    constant!,
    step!,
    pulse!,
    ramp!,
    gamma!,
    sin!,
    exp!,
    simulate,
    mean_firing_rate,
    spikes_num_per_trial,
    interspike_intervals,
    instant_firing_rate,
    poisson_simple,
    poisson_refraction,
    poisson_renewal

include("responses.jl")
include("simple_poisson.jl")

end # module
