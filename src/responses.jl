using Distributions: Gamma, pdf

## Class for creating firing rate function
mutable struct Neuron
    t::AbstractRange    # time vector
    r::AbstractVector   # true instantenous firing rate of a neuron
end

function Neuron(t_start::Real, t_end::Real, Δt::Real, r_const::Real)
    t = t_start:Δt:t_end
    r = fill(r_const, length(t))
    Neuron(t, r)
end
function Neuron(t::AbstractRange, r_const::Real)
    r = fill(r_const, length(t))
    Neuron(t, r)
end


## Response functions
function constant!(n::Neuron, x::Real)
    n.r .+= x
end

function step!(n::Neuron, onset::Real, x::Real; relative::Bool=false)
    # onset - time in s
    # x - by how much firing rate increases after step onseet: either by x in Hz, if relative==true and by a fraction x otherwise
    i = searchsortedfirst(n.t, onset)
    if relative
        n.r[i:end] .*= x
    else
        n.r[i:end] .+= x
    end
end

function pulse!(n::Neuron, onset::Real, offset::Real, x::Real; relative::Bool=false)
    # onset - time in s
    # x - by how much firing rate increases after step onseet: either by x in Hz, if relative==true and by a fraction x otherwise
    i_on = searchsortedfirst(n.t, onset)
    i_off = searchsortedfirst(n.t, offset) - 1
    if relative
        n.r[i_on:i_off] .*= x
    else
        n.r[i_on:i_off] .+= x
    end
end

# Ramp function
function ramp!(n::Neuron, onset::Real, offset::Real, x1::Real, x2::Real)
    # raises or falls linearly from x1 at onset to x2 at offset timepoint
    i_on = searchsortedfirst(n.t, onset)
    i_off = searchsortedfirst(n.t, offset) - 1

    Δt = offset - onset - step(n.t)
    Δx = x2 - x1
    slope = Δx / Δt
    n.r[i_on:i_off] .+= slope * (n.t[i_on:i_off] .- onset) .+ x1
end

# Gamma function
γ(t::Real, k::Real, θ::Real) = pdf(Gamma(k, θ), t)
function gamma!(n::Neuron, onset::Real, k::Real, θ::Real, x::Real)
    # Raises firing rate by gamma distribution of k nd θ
    # k sets the shape of gamma function
    # θ sets width of ggamma function
    # x sets the amplitude of gamma distribution
    i_on = searchsortedfirst(n.t, onset)

    t = n.t[i_on:end] .- onset
    resp = γ.(50t, k, θ)
    max_resp = maximum(resp)
    resp .*= (x / max_resp)
    n.r[i_on:end] += resp
end

# Sinus function
function sin!(n::Neuron, ω::Real, θ::Real, a::Real, b::Real)
    n.r += a .* sin.(ω .* n.t .+ θ) .+ b
end

# Exponential function
function exp!(n::Neuron, onset::Real, α::Real, β::Real, τ::Real)
    i_on = searchsortedfirst(n.t, onset)

    t = n.t[i_on:end] .- onset
    n.r[i_on:end] += α .* ℯ .^ (-t ./ τ) .+ β
end

function simulate(n::Neuron)
    # Make sure there are no negative firing rate values
    idx = findall(λ -> λ < 0, n.r)
    n.r[idx] .= 0.0

    # Return time and firing rate
    n.t, n.r
end
