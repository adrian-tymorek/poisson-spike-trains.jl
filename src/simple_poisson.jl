# Constant firing rate
function poisson_simple(r::Real, n::Integer, Δt::Real, t_start::Real, t_end::Real)
    # Creates aligned spike trains drawn from a homogeneous Poisson proces (i.e. with a CONSTANT firing rate). No relative  refractory period, very basic absolute refractory period
    T = t_end - (t_start - Δt)     # total time of a single trial; in s
    m = Int(ceil(T / Δt))  # number of timepoint in analysed time window
    t = collect(t_start:Δt:t_end)
    x = rand(m,n) .≤ (r * Δt)
    spike_onsets = [t[x[:,i]] for i ∈ 1:n]

    return spike_onsets
end
function poisson_simple(r::Real, t::AbstractRange, n::Integer)
    # Creates aligned spike trains drawn from a homogeneous Poisson proces (i.e. with a CONSTANT firing rate). No relative  refractory period, very basic absolute refractory period
    Δt = step(t)    # a width of time bin wich may only contain one spike (in a way that's also the absolute refractory period
    T = t[end] - (t[begin] - Δt)     # total time of a single trial; in s
    m = Int(ceil(T / Δt))  # number of timepoint in analysed time window
    x = rand(m,n) .≤ (r * Δt)
    spike_onsets = [t[x[:,i]] for i ∈ 1:n]

    return spike_onsets
end

# Variable firing rate
function poisson_simple(r::AbstractVector, t::AbstractRange, n::Integer)
    # Creates aligned spike trains drawn from an imhomogeneous Poisson process (i.e. with a VARIABLE firing rate). No relative  refractory period, very basic absolute refractory period
    Δt = step(t)    # a width of time bin wich may only contain one spike (in a way that's also the absolute refractory period
    T = t[end] - (t[begin] - Δt)     # total time of a single trial; in s
    m = Int(ceil(T / Δt))  # number of timepoint in analysed time window
    x = rand(m,n) .≤ (r .* Δt)
    spike_onsets = [t[x[:,i]] for i ∈ 1:n]

    return spike_onsets
end


function mean_firing_rate(spike_onsets::AbstractVector{T}, time::Real, stim_num::Integer) where T <: AbstractVector
    sum([length(spike_onsets[i]) for i ∈ 1:stim_num] ./ time) / stim_num
end

function spikes_num_per_trial(spike_onsets::AbstractVector{T}, time::Real, stim_num::Integer) where T <: AbstractVector
    spikes_num = [length(spike_onsets[i]) for i ∈ 1:stim_num] ./ time
end

function interspike_intervals(spike_onsets::AbstractVector{T}, stim_num::Integer) where T <: AbstractVector
    isi = vcat([diff(spike_onsets[i]) for i ∈ 1:stim_num]...)
end

function instant_firing_rate(spike_onsets::AbstractVector{T}, t::AbstractVector, n::Integer) where T <: AbstractVector
    Δt = step(t)
    x = zeros(length(t), n)
    for i ∈ 1:n
        # global x, spike_onsets, t
        v = [searchsortedfirst(t, spike_onsets[i][j]) for j ∈ 1:length(spike_onsets[i])]
        x[v,i] .= 1
    end

    y = (sum(x, dims=2) ./ (n * Δt))
end

exp_refract(x, τ)  = 1 - ℯ ^ (-x / τ)
function poisson_refraction(r::AbstractVector, t::AbstractRange, n::Integer, arp::Real, rrp::Real)
    # Creates aligned spike trains drawn from a homogeneous Poisson proces (i.e. with a CONSTANT firing rate). No relative  refractory period, very basic absolute refractory period
    # arp - absolute refractory period (in s)
    # rrp - relative refractory period (in s; after that time neuron's membrane is repolarized to resting potential)
    Δt = step(t)    # a width of time bin wich may only contain one spike (in a way that's also the absolute refractory period
    T = t[end] - (t[begin] - Δt)     # total time of a single trial; in s
    m = Int(ceil(T / Δt))  # number of timepoint per trial
    M = rand(m,n) # .≤ (r * Δt)
    x = zeros(Bool, size(M))
    # Compute indices num for refractory periods
    arp_idx = Int64(ceil(arp / Δt))
    rrt = Δt:Δt:rrp # relative refractory time vector
    rrp_idx = length(rrt)

    # Loop over all trials
    for j ∈ 1:n
        # global M, x, r
        λ = copy(r)
        # i = 1
        # Loop over all timepoints within a trial
        for i ∈ 1:m
            if M[i,j] ≤ λ[i] * Δt
                x[i,j] = true
                # Absolute refractory period - set instantenous firing rate in adjucent timepoints to 0 Hz
                if i+arp_idx ≤ m
                    λ[i+1:i+arp_idx] .= 0.0
                else
                    λ[i+1:end] .= 0.0
                    break
                end

                # Relative  refractory period - model by exponential return from zero to original firing rate
                if i+arp_idx+rrp_idx ≤ m
                    λ[i+arp_idx+1:i+arp_idx+rrp_idx] = exp_refract.(λ[i+arp_idx+1:i+arp_idx+rrp_idx], 0.15rrp)
                else
                    λ[i+arp_idx+1:end] = exp_refract.(λ[i+arp_idx+1:end], 0.15rrp)
                    break
                end
            end
        end
    end

    # Retrieve spike onsets
    spike_onsets = [t[x[:,i]] for i ∈ 1:n]
end


# Renewal process model
function poisson_renewal(r::Real, t::AbstractVector, n::Integer, k::Integer)
    # r - firing rate
    # t - time vector
    # n - number os simulations
    # k - take onlny every k-th spike; equivalent to k parameter in gammba distribution function (inter-spike interval will follow gamma distribution function with k=k)

    # Create a standard Poisson point process model of a spike train
    spike_onsets = poisson_simple(r * k, t, n)

    # Leave only every k-th spike
    for i ∈ 1:n
        j = rand(1:k)
        spike_onsets[i] = spike_onsets[i][j:k:end]
    end
    # spike_onsets = [spike_onsets[i][rand(1:k):k:end] for i ∈ 1:n]

    return spike_onsets
end

function poisson_renewal(r::AbstractVector, t::AbstractVector, n::Integer, k::Integer)
    # r - firing rate
    # t - time vector
    # n - number os simulations
    # k - take onlny every k-th spike; equivalent to k parameter in gammba distribution function (inter-spike interval will follow gamma distribution function with k=k)

    # Create a standard Poisson point process model of a spike train
    spike_onsets = poisson_simple(r .* k, t, n)

    # Leave only every k-th spike
    for i ∈ 1:n
        j = rand(1:k)
        spike_onsets[i] = spike_onsets[i][j:k:end]
    end
    # spike_onsets = [spike_onsets[i][rand(1:k):k:end] for i ∈ 1:n]

    return spike_onsets
end
