### A Pluto.jl notebook ###
# v0.12.17

using Markdown
using InteractiveUtils

# ╔═╡ 329f27a4-54bc-11eb-3cc3-0d41ee3c2560
using PoissonSpikeTrains

# ╔═╡ f7687e72-54c2-11eb-1fd6-0dbe9f5977c1
using Plots

# ╔═╡ 43563b4a-54bd-11eb-1942-034d01930236
# Load raster_plot function
include("./src/raster_plot.jl");

# ╔═╡ 740ad71a-54bc-11eb-24d1-6b1de705a016
md"### Import packges"

# ╔═╡ 30d47950-54bd-11eb-0cdb-11624f7ad240
md"# Let us simulate a 2 s spike train with baseline of 4 Hz and a pulse response of amplitude 10 Hz, starting at t = 0 s"

# ╔═╡ 83826d9c-54bd-11eb-3325-c7bf94a2ed67
begin 
	Δt = 1e-4      # min. distance between spikes
	t_start = -1.0 # in s
	t_end = 1.0    # in s
	baseline = 4.0 # in Hz
end;

# ╔═╡ 3a722014-54bf-11eb-163d-7935fe84af5d
md"### Create a neuron with just a baseline firing rate"

# ╔═╡ 9fcc7df2-54be-11eb-0ea9-219a0a2b10c6
neuron = Neuron(t_start, t_end, Δt, baseline);

# ╔═╡ a765caae-54bf-11eb-307d-1d511a479b1a
md"### Add a 4 Hz pulse respone at"

# ╔═╡ cea74db8-54bf-11eb-3a98-c1323ec393ac
begin
	resp_onset = 0.0 # in s
	resp_offset = 0.1 # in s
	resp_amplitude = 10.0 # in Hz
	pulse!(neuron, resp_onset, resp_offset, resp_amplitude) # add pulse response
	t, r = neuron.t, neuron.r  # retreive time and instantaneous firing rate 
end;

# ╔═╡ 977b1af8-54c0-11eb-38c5-d1b3d1ffffd1
md"### Display the instantaneous firing rate"

# ╔═╡ 16ec40ac-54bf-11eb-2092-1faad8f5dfb3
begin plt = plot(t, r; legend=nothing)
    ylims!(0.0, 1.1 * maximum(r))
    xlabel!("Time [s]")     
    ylabel!("Firing rate [Hz]")
    plt
end

# ╔═╡ c58494d8-54c0-11eb-3732-914279ecb47b
md"### Build a single renewal model spike train for k parameter of 4"

# ╔═╡ d453d69a-54c0-11eb-30e6-ed6cbb97c113
begin 
	n1 = 1 # number of spike trains/number of trials
	k1 = 4 # Leave only every k-th spike
	spike_train_1 = poisson_renewal(r, t, n1, k1)
end;

# ╔═╡ 379a35f0-54c1-11eb-03fe-353ef9b81444
md"### Let us see the spike train"

# ╔═╡ 509471ce-54c1-11eb-2643-19724443489c
raster_plot(spike_train_1; markersize=2.0)

# ╔═╡ 79329c94-54c1-11eb-3da3-e33e140636a9
md"### For a single spike train we cannot really tell whether our model works well. Let us generate 100 spike trains"

# ╔═╡ c8d6920a-54c1-11eb-1a5e-e5c15882424a
begin 
	n_2 = 100 # number of spike trains/number of trials
	k_2 = 4 # Leave only every k-th spike
	spike_train_2 = poisson_renewal(r, t, n_2, k_2)
end;

# ╔═╡ ee3cf414-54c1-11eb-27cf-59f076a77470
raster_plot(spike_train_2; markersize=2.0)

# ╔═╡ Cell order:
# ╟─740ad71a-54bc-11eb-24d1-6b1de705a016
# ╠═329f27a4-54bc-11eb-3cc3-0d41ee3c2560
# ╠═f7687e72-54c2-11eb-1fd6-0dbe9f5977c1
# ╠═43563b4a-54bd-11eb-1942-034d01930236
# ╠═30d47950-54bd-11eb-0cdb-11624f7ad240
# ╠═83826d9c-54bd-11eb-3325-c7bf94a2ed67
# ╠═3a722014-54bf-11eb-163d-7935fe84af5d
# ╠═9fcc7df2-54be-11eb-0ea9-219a0a2b10c6
# ╟─a765caae-54bf-11eb-307d-1d511a479b1a
# ╠═cea74db8-54bf-11eb-3a98-c1323ec393ac
# ╟─977b1af8-54c0-11eb-38c5-d1b3d1ffffd1
# ╠═16ec40ac-54bf-11eb-2092-1faad8f5dfb3
# ╟─c58494d8-54c0-11eb-3732-914279ecb47b
# ╠═d453d69a-54c0-11eb-30e6-ed6cbb97c113
# ╟─379a35f0-54c1-11eb-03fe-353ef9b81444
# ╠═509471ce-54c1-11eb-2643-19724443489c
# ╟─79329c94-54c1-11eb-3da3-e33e140636a9
# ╠═c8d6920a-54c1-11eb-1a5e-e5c15882424a
# ╠═ee3cf414-54c1-11eb-27cf-59f076a77470
