# Usage

All of the examples are written in [Pluto notebooks](https://github.com/fonsp/Pluto.jl).
To use them, you need to first [install Pluto](https://github.com/fonsp/Pluto.jl#installation).
Then in your Julia respl type
```julia
julia> import Pluto
julia> Pluto.run()
```

 Pluto will open in you web browser and you can navigate to a chosen example notebook.
