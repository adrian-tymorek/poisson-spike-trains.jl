"""
    raster_plot(spike_onsets; color=:black, markersize=2, markerstyle=:dot)

Builds and displays a peri-stimulus raster plot from aligned spikes.

# Examples
```julia-repl
julia> spike_onsets, stim_onsets, Δt = load_data("/path/to/MAT/file.mat", get_Δt=true)
julia> aligned = align(spike_onsets, stim_onsets, -2.0, 2.0)
julia> raster_plot(aligned)
```
"""
function raster_plot(spike_onsets::AbstractVector{S}; color=:black, markersize=2, markerstyle=:dot) where S <: AbstractVector
    T = [i .* ones(Int64, length(spike_onsets[i])) for i ∈ 1:length(spike_onsets)]
    plt = scatter(spike_onsets, T; legend=nothing, color=color, marker=(markersize, ));
    xlabel!("Time [s]")
    ylabel!("Trial number")
    # xlims!(t_start-0.002, t_end+0.002);

    return plt
end
